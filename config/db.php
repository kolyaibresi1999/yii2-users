<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . self::env('DB_HOST') . ';dbname=' . self::env('DB_NAME'),
    'username' => self::env('DB_USER'),
    'password' => self::env('DB_PASSWORD'),
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
