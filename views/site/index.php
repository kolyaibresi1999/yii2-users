<?php

use yii\helpers\Url;

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Добро пожаловать!</h1>

        <p class="lead">Я решил выполнить эту задачу двумя способами.</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6 mb-3">
                <h2>С помощью gii</h2>

                <p>У yii2 есть gii - это конструктор, когда нужно что то сделать очень быстро это очень даже не плохое решение при условии что в БД все норм</p>

                <p><a class="btn btn-outline-secondary" href="<?= Url::to(['contact/index']); ?>">Страница с решением с gii &raquo;</a></p>
            </div>
            <div class="col-lg-6">
                <h2>Ручками</h2>

                <p>Создал еще один контроллер модель и таблица общая <br>для обоих решений </p>

                <p><a class="btn btn-outline-secondary" href="<?= Url::to(['contact-manual/index']); ?>">Страница с другим решением &raquo;</a></p>
            </div>
        </div>

    </div>
</div>