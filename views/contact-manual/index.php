<?php

use yii\helpers\Url;
use yii\bootstrap5\LinkPager;
?>
<button type="button" class="btn btn-primary mt-3 mb-3" data-bs-toggle="modal" data-bs-target="#modal-contact" data-bs-action="create" data-bs-url="<?= Url::to(['contact-manual/create']); ?>">Добавить новый контакт</button>
<table class="table">
    <thead class="table-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">ФИО</th>
            <th scope="col">Email</th>
            <th scope="col">Телефон</th>
            <th scope="col">Действия</th>
        </tr>
    </thead>
    <tbody id="table-contacts">
        <?php foreach ($сontacts as $сontact) : ?>
            <tr id="contact-id<?= \yii\helpers\Html::encode($сontact->id)  ?>">
                <td><?= \yii\helpers\Html::encode($сontact->id) ?></td>
                <td><?= \yii\helpers\Html::encode($сontact->fullName()) ?></td>
                <td><?= \yii\helpers\Html::encode($сontact->email) ?></td>
                <td><?= \yii\helpers\Html::encode($сontact->cellphone) ?></td>
                <td>
                    <button type="button" class="btn btn-primary" style="width: max-content;" data-bs-toggle="modal" data-bs-action="update" data-bs-id="<?= $сontact->id ?>" data-bs-lastname="<?= \yii\helpers\Html::encode($сontact->lastname) ?>" data-bs-firstname="<?= \yii\helpers\Html::encode($сontact->firstname) ?>" data-bs-middlename="<?= \yii\helpers\Html::encode($сontact->middlename) ?>" data-bs-email="<?= \yii\helpers\Html::encode($сontact->email) ?>" data-bs-cellphone="<?= \yii\helpers\Html::encode($сontact->cellphone) ?>" data-bs-status="<?= \yii\helpers\Html::encode($сontact->status) ?>" data-bs-url="<?= Url::to(['contact-manual/update', 'id' => $сontact->id]); ?>" data-bs-target="#modal-contact">Редактировать</button>
                    <button class="btn btn-danger" onclick="deleteRow('<?= Url::to(['contact-manual/delete', 'id' => $сontact->id]); ?>', '<?= $сontact->id ?>')">Удалить</button>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?=
LinkPager::widget([
    'pagination' => $pagination,
]);
?>
<?= $this->render('modal-form') ?>

<script>
    let page = '<?= isset($_GET['page']) ? $_GET['page'] : 1 ?>';
</script>
<?php $this->registerJsFile("@web/js/contact.js", [
    'depends' => [
        \yii\web\JqueryAsset::className()
    ]
]); ?>