<?php

use yii\helpers\Html;

?>
<div class="modal fade" id="modal-contact" tabindex="-1" aria-labelledby="modal-contact-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-contact-label">Создать новый контакт</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="post" id="contact-form">

                    <?= Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>
                    <input type="hidden" id="contact-id" name="Contact[id]">

                    <div class="mb-3 field-contact-lastname">
                        <label class="form-label" for="contact-lastname">Фамилия</label>
                        <input type="text" id="contact-lastname" class="form-control" name="Contact[lastname]" maxlength="32">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="mb-3 field-contact-firstname">
                        <label class="form-label" for="contact-firstname">Имя</label>
                        <input type="text" id="contact-firstname" class="form-control" name="Contact[firstname]" maxlength="32">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="mb-3 field-contact-middlename">
                        <label class="form-label" for="contact-middlename">Отчество</label>
                        <input type="text" id="contact-middlename" class="form-control" name="Contact[middlename]" maxlength="32">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="mb-3 field-contact-email">
                        <label class="form-label" for="contact-email">Email</label>
                        <input type="text" id="contact-email" class="form-control" name="Contact[email]" maxlength="32">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="mb-3 field-contact-cellphone">
                        <label class="form-label" for="contact-cellphone">Сотовый телефон</label>
                        <input type="text" id="contact-cellphone" class="form-control" name="Contact[cellphone]" maxlength="32">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="mb-3 field-contact-status">
                        <label class="form-label" for="contact-status">Статус</label>
                        <select id="contact-status" class="form-select" name="Contact[status]">
                            <option value="1">Активен</option>
                            <option value="0">Удален</option>
                        </select>
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button id="modal-submit-button" type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>