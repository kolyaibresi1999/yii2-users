<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string|null $lastname
 * @property string|null $firstname
 * @property string|null $middlename
 * @property string|null $email
 * @property string|null $cellphone
 * @property int|null $status
 */
class Contact extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['lastname', 'firstname', 'middlename', 'email', 'cellphone'], 'string', 'max' => 32],
            [['lastname', 'firstname', 'middlename', 'status', 'email', 'cellphone'], 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lastname' => 'Фамилия',
            'firstname' => 'Имя',
            'middlename' => 'Отчество',
            'email' => 'Email',
            'cellphone' => 'Сотовый телефон',
            'status' => 'Статус',
        ];
    }

    public function fullName()
    {
        return $this->lastname . ' ' . $this->firstname . ' ' . $this->middlename;
    }
}
