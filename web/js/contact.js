let modalContact = document.getElementById('modal-contact')
let modalContactTitle = document.getElementById('modal-contact-label')
let modalContactForm = document.getElementById('contact-form')
let contactLastnameInput = document.getElementById('contact-lastname')
let contactFirstnameInput = document.getElementById('contact-firstname')
let contactMiddlenameInput = document.getElementById('contact-middlename')
let contactEmailInput = document.getElementById('contact-email')
let contactCellphoneInput = document.getElementById('contact-cellphone')
let contactStatusInput = document.getElementById('contact-status')
let contactIdInput = document.getElementById('contact-id')

modalContact.addEventListener('show.bs.modal', event => {
    let button = event.relatedTarget
    let action = button.getAttribute('data-bs-action')
    let url = button.getAttribute('data-bs-url')
    if (action == 'update') {
        let contactLastname = button.getAttribute('data-bs-lastname')
        let contactFirstname = button.getAttribute('data-bs-firstname')
        let contactMiddlename = button.getAttribute('data-bs-middlename')
        let contactEmail = button.getAttribute('data-bs-email')
        let contactCellphone = button.getAttribute('data-bs-cellphone')
        let contactStatus = button.getAttribute('data-bs-status')
        let contactId = button.getAttribute('data-bs-id')

        contactLastnameInput.value = contactLastname;
        contactFirstnameInput.value = contactFirstname;
        contactMiddlenameInput.value = contactMiddlename;
        contactEmailInput.value = contactEmail;
        contactCellphoneInput.value = contactCellphone;
        contactStatusInput.value = contactStatus;
        contactIdInput.value = contactId;
        modalContactTitle.innerHTML = "Редактировать контакт"
        modalContactForm.setAttribute('action', url + '&page=' + page);
    } else {
        modalContactForm.setAttribute('action', url + '?page=' + page);
    }
})

modalContact.addEventListener('hidden.bs.modal', event => {
    modalContactTitle.innerHTML = "Создать новый контакт"
    contactLastnameInput.value = '';
    contactFirstnameInput.value = '';
    contactMiddlenameInput.value = '';
    contactEmailInput.value = '';
    contactCellphoneInput.value = '';
    contactStatusInput.value = '';
    contactIdInput.value = '';
    modalContactForm.removeAttribute('action');
    contactLastnameInput.classList.remove("is-invalid")
    contactLastnameInput.nextElementSibling.innerHTML = '';
    contactFirstnameInput.classList.remove("is-invalid")
    contactFirstnameInput.nextElementSibling.innerHTML = '';
    contactMiddlenameInput.classList.remove("is-invalid")
    contactMiddlenameInput.nextElementSibling.innerHTML = '';
    contactEmailInput.classList.remove("is-invalid")
    contactEmailInput.nextElementSibling.innerHTML = '';
    contactCellphoneInput.classList.remove("is-invalid")
    contactCellphoneInput.nextElementSibling.innerHTML = '';
    contactStatusInput.classList.remove("is-invalid")
    contactStatusInput.nextElementSibling.innerHTML = '';
})

function writeErrors(errors) {
    $.each(errors, function (key, value) {
        if (key == 'lastname') {
            contactLastnameInput.classList.add("is-invalid")
            contactLastnameInput.nextElementSibling.innerHTML = value;
        }
        if (key == 'firstname') {
            contactFirstnameInput.classList.add("is-invalid")
            contactFirstnameInput.nextElementSibling.innerHTML = value;
        }
        if (key == 'middlename') {
            contactMiddlenameInput.classList.add("is-invalid")
            contactMiddlenameInput.nextElementSibling.innerHTML = value;
        }
        if (key == 'email') {
            contactEmailInput.classList.add("is-invalid")
            contactEmailInput.nextElementSibling.innerHTML = value;
        }
        if (key == 'cellphone') {
            contactCellphoneInput.classList.add("is-invalid")
            contactCellphoneInput.nextElementSibling.innerHTML = value;
        }
        if (key == 'status') {
            contactStatusInput.classList.add("is-invalid")
            contactStatusInput.nextElementSibling.innerHTML = value;
        }
    });
}

function cleanErrors() {
    contactLastnameInput.classList.remove("is-invalid")
    contactLastnameInput.nextElementSibling.innerHTML = '';
    contactFirstnameInput.classList.remove("is-invalid")
    contactFirstnameInput.nextElementSibling.innerHTML = '';
    contactMiddlenameInput.classList.remove("is-invalid")
    contactMiddlenameInput.nextElementSibling.innerHTML = '';
    contactEmailInput.classList.remove("is-invalid")
    contactEmailInput.nextElementSibling.innerHTML = '';
    contactCellphoneInput.classList.remove("is-invalid")
    contactCellphoneInput.nextElementSibling.innerHTML = '';
    contactStatusInput.classList.remove("is-invalid")
    contactStatusInput.nextElementSibling.innerHTML = '';
}

function updateRows(contacts) {
    let tableBody = document.getElementById('table-contacts');
    tableBody.innerHTML = ''
    contacts.forEach(contact => {
        const row = document.createElement('tr');
        row.setAttribute('id', "contact-id" + contact.id);
        const idCell = document.createElement('td');

        idCell.innerText = contact.id;
        row.appendChild(idCell);

        const nameCell = document.createElement('td');
        nameCell.innerText = contact.lastname + " " + contact.firstname + " " + contact.middlename;
        row.appendChild(nameCell);

        const emailCell = document.createElement('td');
        emailCell.innerText = contact.email;
        row.appendChild(emailCell);

        const cellphoneCell = document.createElement('td');
        cellphoneCell.innerText = contact.cellphone;
        row.appendChild(cellphoneCell);

        const buttonCell = document.createElement('td');
        const editButton = document.createElement('button');
        editButton.type = 'button';
        editButton.classList.add('btn', 'btn-primary');
        editButton.style.width = 'max-content';
        editButton.dataset.bsToggle = 'modal';
        editButton.dataset.bsAction = 'update';
        editButton.dataset.bsId = contact.id;
        editButton.dataset.bsLastname = contact.lastname;
        editButton.dataset.bsFirstname = contact.firstname;
        editButton.dataset.bsMiddlename = contact.middlename;
        editButton.dataset.bsEmail = contact.email;
        editButton.dataset.bsCellphone = contact.cellphone;
        editButton.dataset.bsStatus = contact.status;
        editButton.dataset.bsUrl = '/contact-manual/update?id=' + contact.id;
        editButton.dataset.bsTarget = '#modal-contact';
        editButton.innerText = 'Редактировать';
        buttonCell.appendChild(editButton);

        const deleteButton = document.createElement('button');
        deleteButton.classList.add('btn', 'btn-danger');
        deleteButton.innerText = 'Удалить';
        deleteButton.setAttribute('onclick', "deleteRow('/contact-manual/delete?id=" + contact.id + "'," + contact.id + ")");

        buttonCell.appendChild(deleteButton);

        row.appendChild(buttonCell);

        tableBody.appendChild(row);
    });
}

function closeModal() {
    let modalInstance = bootstrap.Modal.getInstance(modalContact);
    modalInstance.hide();
}

modalContactForm.addEventListener('submit', function (e) {
    e.preventDefault();
    cleanErrors()
    let form = $(this);
    let formData = form.serialize();
    $.ajax({
        url: form.attr("action"),
        type: form.attr("method"),
        data: formData,
        success: function (data) {
            if (data.status == 'success') {
                updateRows(data.contacts)
                closeModal()
            } else {
                writeErrors(data.model)
            }
        },

        error: function () {

            alert("Something went wrong");

        }
    });
});


function deleteRow(url, id) {
    $.ajax({
        url: url,
        method: 'post',
        dataType: 'html',
        data: { id: id },
        success: function (data) {
            $("#contact-id" + id).remove();
        }
    });
}